package class_solution;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class National {
	private final LocalDate birthDate;
	private final int sequentialNr;
	private final int cd;


	public National(LocalDate birthDate, int sequentialNr, int cd) {
		this.birthDate = birthDate;
		this.sequentialNr = sequentialNr;
		this.cd = cd;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public int getSequentialNr() {
		return sequentialNr;
	}

	public int getCd() {
		return cd;
	}

	@Override
	public String toString() {
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yy.MM.dd");
		String formattedDate = birthDate.format(dateFormatter);
		return String.format("%s-%d.%d", formattedDate, getSequentialNr(), getCd());
	}
}
