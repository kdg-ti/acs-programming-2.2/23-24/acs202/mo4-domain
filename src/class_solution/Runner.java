package class_solution;

import java.time.LocalDate;

public class Runner {
	public static void main(String[] args) {
		System.out.println(new NationalRecord(
			LocalDate.of(1967,6,3),121,63)
		);
	}
}
