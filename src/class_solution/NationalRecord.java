package class_solution;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public record NationalRecord(LocalDate birthDate,int sequentialNr,int cd ) {

	public NationalRecord{
		if (sequentialNr < 0 || sequentialNr > 1000) {
			throw new IllegalArgumentException(String.format("SequenceNumber %d must be between 0 and 999", sequentialNr));
		}
	}

	@Override
	public String toString() {
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yy.MM.dd");
		String formattedDate = birthDate.format(dateFormatter);
		return String.format("%s-%d.%d", formattedDate, sequentialNr, cd);
	}
}
